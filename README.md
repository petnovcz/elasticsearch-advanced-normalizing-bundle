## Elasticsearch advanced normalizing bundle

This elasticsearch plugin provides tools for advanced normalization as
 - standart tokenization (via standard_tokenizer_filter)
 - hunspell stemming (via hunspell_normalizing token filter) 
 - fingerprint (via fingerprint token filter)
 
### Usage
If you want advanced normalizer, this plugin allows you to:
 - tokenize the string with standard tokeznier token filter
 - apply filters... e.g. attached hunspell_normalizing
 - fingerprint to concat tokens into normalized single token 

### How to build
```shell script
./gradlew clean && ./gradlew assemble
```

### How to install
```shell script
/usr/share/elasticsearch/bin/elasticsearch-plugin install file:///[path_to_build]/advanced-normalizing-bundle-7.8.0.zip
```

### Example
```json
{
  "index": {
    "analysis": {
      "normalizer": {
        "my_normalizer": {
          "type": "custom",
          "filter": [
            "standard_tokenizer",
            "hunspell_en",
            "fingerprint_normalizing"
          ]
        }
      },
      "filter": {
        "standard_tokenizer": {
          "type": "standard_tokenizer_filter"
        },
        "hunspell_en": {
          "type": "hunspell_normalizing",
          "locale": "en_US"
        }
      }
    }
  }
}
```

