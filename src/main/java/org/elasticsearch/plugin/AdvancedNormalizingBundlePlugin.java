/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elasticsearch.plugin;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.util.SetOnce;
import org.elasticsearch.analysis.common.SynonymGraphTokenFilterFactory;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.IndexNameExpressionResolver;
import org.elasticsearch.cluster.service.ClusterService;
import org.elasticsearch.common.io.stream.NamedWriteableRegistry;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.NamedXContentRegistry;
import org.elasticsearch.env.Environment;
import org.elasticsearch.env.NodeEnvironment;
import org.elasticsearch.index.IndexSettings;
import org.elasticsearch.index.analysis.*;
import org.elasticsearch.indices.analysis.AnalysisModule;
import org.elasticsearch.indices.analysis.HunspellService;
import org.elasticsearch.plugins.AnalysisPlugin;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.repositories.RepositoriesService;
import org.elasticsearch.script.ScriptService;
import org.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.watcher.ResourceWatcherService;

import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;


public class AdvancedNormalizingBundlePlugin extends Plugin implements AnalysisPlugin {

    private final SetOnce<HunspellService> hunspellService = new SetOnce<>();

    private static final Logger logger = LogManager.getLogger(AdvancedNormalizingBundlePlugin.class);

    @Override
    public Collection<Object> createComponents(Client client, ClusterService clusterService, ThreadPool threadPool,
                                               ResourceWatcherService resourceWatcherService, ScriptService scriptService,
                                               NamedXContentRegistry xContentRegistry, Environment environment,
                                               NodeEnvironment nodeEnvironment, NamedWriteableRegistry namedWriteableRegistry,
                                               IndexNameExpressionResolver expressionResolver,
                                               Supplier<RepositoriesService> repositoriesServiceSupplier) {

        this.hunspellService.trySet(this.createHunspellService(environment));
        return Collections.emptyList();
    }

    private HunspellService createHunspellService(Environment env) {
        try {
            HunspellService hunspell = new HunspellService(env.settings(), env, Collections.emptyMap());
            logger.info("loaded Hunspell serivce for normalizing plugin");
            return hunspell;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Map<String, AnalysisModule.AnalysisProvider<TokenFilterFactory>> getTokenFilters() {
        Map<String, AnalysisModule.AnalysisProvider<TokenFilterFactory>> extra = new HashMap<>();
        extra.put("standard_tokenizer_filter", StandardTokenizerTokenFilterFactory::new);
        extra.put("fingerprint_normalizing", FingerprintNormalizingTokenFilterFactory::new);
        extra.put("hunspell_normalizing", AnalysisPlugin.requiresAnalysisSettings((IndexSettings indexSettings, Environment environment, String name, Settings settings) -> {
            if (this.hunspellService.get() == null) {
                this.hunspellService.trySet(this.createHunspellService(environment));
            }
            return new HunspellNormalizingTokenFilterFactory(indexSettings, name, settings, hunspellService.get());
        }));

        // extra.put("synonym_graph_normalizing", AnalysisPlugin.requiresAnalysisSettings(SynonymGraphNormalizingTokenFilterFactory::new));

        return extra;
    }
}
