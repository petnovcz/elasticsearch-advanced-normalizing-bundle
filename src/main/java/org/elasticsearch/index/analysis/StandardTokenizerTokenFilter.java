/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.index.analysis;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizerImpl;
import org.apache.lucene.analysis.tokenattributes.*;


public final class StandardTokenizerTokenFilter extends TokenFilter {
    /** A private instance of the JFlex-constructed scanner */
    private StandardTokenizerImpl scanner;

    private int skippedPositions;

    private Reader currentTokenInput;

    private State state;

    /**
     * @param input TokenStream
     */
    public StandardTokenizerTokenFilter(TokenStream input) {
        super(input);
        init();
    }

    private void init() {
        this.scanner = new StandardTokenizerImpl(ILLEGAL_STATE_READER);
    }

    // this tokenizer generates three attributes:
    // term offset, positionIncrement and type
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
    private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    private final TypeAttribute typeAtt = addAttribute(TypeAttribute.class);

    private char[] curTermBuffer;

    private static final Reader ILLEGAL_STATE_READER = new Reader() {
        @Override
        public int read(char[] cbuf, int off, int len) {
            throw new IllegalStateException("TokenStream contract violation: reset()/close() call missing, " +
                    "reset() called multiple times, or subclass does not call super.reset().");
        }

        @Override
        public void close() {}
    };

    /*
     * (non-Javadoc)
     *
     * @see org.apache.lucene.analysis.TokenStream#next()
     */
    @Override
    public final boolean incrementToken() throws IOException {
        clearAttributes();
        skippedPositions = 0;

        while(true) {
            if (curTermBuffer == null) {
                if (!input.incrementToken()) {
                    return false;
                }

                if (state == null && termAtt.length() == 0) {
                    return true;
                }
                state = captureState();
                curTermBuffer = termAtt.buffer().clone();
                currentTokenInput = new StringReader(termAtt.toString());
                this.scanner.yyreset(currentTokenInput);
            }

            int tokenType = scanner.getNextToken();
            if (tokenType == StandardTokenizerImpl.YYEOF) {
                curTermBuffer = null;
                return false;
            }

            if (scanner.yylength() <= StandardAnalyzer.DEFAULT_MAX_TOKEN_LENGTH) {
                posIncrAtt.setPositionIncrement(skippedPositions + 1);
                scanner.getText(termAtt);
                final int start = scanner.yychar();
                int startOffset = correctOffset(start);
                int endOffset = correctOffset(start + termAtt.length());
                offsetAtt.setOffset(startOffset, endOffset);
                typeAtt.setType(StandardTokenizer.TOKEN_TYPES[tokenType]);
                termAtt.copyBuffer(curTermBuffer, startOffset, endOffset - startOffset);
                return true;
            } else {
                // When we skip a too-long term, we still increment the
                // position increment
                skippedPositions++;
            }
        }
    }

    @Override
    public final void end() throws IOException {
        super.end();
        // set final offset
        int finalOffset = correctOffset(scanner.yychar() + scanner.yylength());
        offsetAtt.setOffset(finalOffset, finalOffset);
        // adjust any skipped tokens
        posIncrAtt.setPositionIncrement(posIncrAtt.getPositionIncrement()+skippedPositions);
    }

    @Override
    public void close() throws IOException {
        super.close();
        scanner.yyreset(currentTokenInput);
    }

    @Override
    public void reset() throws IOException {
        super.reset();
        skippedPositions = 0;
    }

    protected final int correctOffset(int currentOff) {
        return (currentTokenInput instanceof CharFilter) ? ((CharFilter) currentTokenInput).correctOffset(currentOff) : currentOff;
    }
}
