package org.elasticsearch.index.analysis;

import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.elasticsearch.analysis.common.CommonAnalysisPlugin;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.plugin.AdvancedNormalizingBundlePlugin;
import org.elasticsearch.test.ESTestCase;
import org.elasticsearch.test.ESTokenStreamTestCase;
import org.elasticsearch.cluster.metadata.IndexMetadata;
import org.elasticsearch.Version;

import org.apache.lucene.analysis.TokenStream;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.env.Environment;


public class AdvancedNormalizingBundleTest extends ESTokenStreamTestCase {

    public void testAdvancedNormalizingBundleSimple() throws IOException {

        ESTestCase.TestAnalysis analysis = createAnalysis();

        String source = "duplo-ABNORMALLY";
        String expected = "abnormalduplo";

        NamedAnalyzer normalizer = analysis.indexAnalyzers.getNormalizer("my_normalizer");
        TokenStream ts = normalizer.tokenStream("test", new StringReader(source));

        List<String> terms = new ArrayList<>();
        ts.reset();
        CheckClearAttributesAttribute checkClearAtt = ts.addAttribute(CheckClearAttributesAttribute.class);
        CharTermAttribute termAtt;
        termAtt = ts.getAttribute(CharTermAttribute.class);

        for (int i = 0; i < 10; i++) {
            ts.clearAttributes();

            checkClearAtt.getAndResetClearCalled(); // reset it, because we called clearAttribute() before
            if (ts.incrementToken()) {
                terms.add(termAtt.toString());
            }
        }
        assertEquals(expected, terms.get(0));
    }

    public ESTestCase.TestAnalysis createAnalysis() throws IOException {
        Path home = createTempDir();

        String path = "/org/elasticsearch/index/analysis/index.json";

        Settings settings = Settings.builder().loadFromStream(path, getClass().getResourceAsStream(path), false)
                .put(Environment.PATH_HOME_SETTING.getKey(), createTempDir().toString())
                .put(IndexMetadata.SETTING_VERSION_CREATED, Version.CURRENT)
                .put(Environment.PATH_HOME_SETTING.getKey(), home)
                .build();

        ESTestCase.TestAnalysis analysis =
                AnalysisTestsHelper.createTestAnalysisFromSettings(settings, getDataPath("/indices/analyze/conf_dir"), new AdvancedNormalizingBundlePlugin(), new CommonAnalysisPlugin());

        return analysis;
    }
}